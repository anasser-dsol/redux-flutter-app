import 'package:flutter/material.dart';

class ArticleListPage extends StatelessWidget {

  static const routeName = '/';
  const ArticleListPage({super.key});


  @override
  Widget build(BuildContext context) {
    //Todo: List item click should open edit.
    return Scaffold(
      appBar: AppBar(
        title: const Text('Articles List'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/add');
        },
        child: const Icon(Icons.add),
      )
    );
  }
}
